<div class="container $ExtraClass">
	<% if $ShowTitle %><h2 class="<% if $TitleCenter %>text-center<% end_if %><% if $HSize && $HSize != normal %> $HSize<% end_if %>"<% if $AnchorTag %> id="$AnchorTag"<% end_if %>>$Title</h2><% end_if %>
	<% if $Content %>$Content<% end_if %>
    <div class="row">
        <% loop $Reviews %>
            <div class=" col-12 col-md-4">
                <p>$Name</p>
                <p>$Comment</p>
                <p>Cijfer: $Rating</p>
            </div>
        <% end_loop %>
    </div>
</div>
