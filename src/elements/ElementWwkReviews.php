<?php

namespace Hestec\WebwinkelKeur;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\NumericField;

class ElementWwkReviews extends BaseElement
{

    private static $table_name = 'HestecElementWwkReviews';

    private static $singular_name = 'WwkReview';

    private static $plural_name = 'WwkReviews';

    private static $description = 'Element for WebwinkelKeur reviews';

    private static $icon = 'font-icon-chat';

    private static $db = [
        'Content' => 'HTMLText',
        'Limit' => 'Int'
    ];

    private static $has_many = array(
    );

    //private static $inline_editable = false;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $ContentField = HTMLEditorField::create('Content', "Content");
        $ContentField->setRows(5);
        $LimitField = NumericField::create('Limit', "Limit");
        $LimitField->setDescription("Standaard 3 (0 = 3).");

        $fields->addFieldsToTab('Root.Main', Array(
            $ContentField,
            $LimitField
        ));

        return $fields;
    }

    public function Reviews()
    {

        $limit = $this->Limit;
        if ($this->Limit == 0){
            $limit = 3;
        }

        return Review::get()->filter('Enabled', true)->sort('DateTime', 'DESC')->limit($limit);

    }

    public function getType()
    {
        return 'WwkReviews';
    }
}