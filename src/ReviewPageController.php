<?php

namespace Hestec\WebwinkelKeur;

use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\HiddenField;
use SilverStripe\View\Requirements;
use SilverStripe\Core\Config\Config;
use WebwinkelKeur\Client;
use WebwinkelKeur\Client\Request;
use SilverStripe\Control\Director;

class ReviewPageController extends \PageController {

    private static $allowed_actions = array (
        'ReviewInviteForm',
        'ReviewInvite'
    );

    protected function init() {
        parent::init();

        if ( Director::isLive() ){
            Requirements::javascript('hestec/silverstripe-webwinkelkeur: client/javascript/webwinkelkeur.min.js');
        }else{
            Requirements::javascript('hestec/silverstripe-webwinkelkeur: client/javascript/webwinkelkeur.js');
        }

    }

    public function ReviewInviteForm(){

        $NameField = TextField::create('Name', false);
        $NameField->setAttribute('placeholder', _t("WebwinkelKeur.NAME", "Name"));
        $EmailField = EmailField::create('Email', false);
        $EmailField->setAttribute('placeholder', _t("WebwinkelKeur.EMAIL", "Email"));
        $LinkField = HiddenField::create('Link', 'Link', $this->Link());

        $Action = FormAction::create('SubmitReviewInviteForm', _t("WebwinkelKeur.INVITATION", "Send invitation"));

        $fields = FieldList::create(array(
            $NameField,
            $EmailField,
            $LinkField
        ));

        $actions = FieldList::create(
            $Action
        );

        $form = Form::create($this, __FUNCTION__, $fields, $actions);

        return $form;

    }

    public function SubmitReviewInviteForm($data,$form)
    {

        return $this->redirectBack();

    }

    public function ReviewInvite()
    {

        $apikey = Config::inst()->get('WebwinkelKeur', 'apikey');
        $shopid = Config::inst()->get('WebwinkelKeur', 'shopid');

        $webwinkelKeurClient = new Client($shopid, $apikey);

        $invitation = new Request\Invitation();
        $invitation
            ->setCustomerName($_GET['name'])
            ->setEmailAddress($_GET['email']);

        try {
            $webwinkelKeurClient->sendInvitation($invitation);
            $result = "success";
        } catch (Client\Exception $e) {
            //echo $e->getMessage();
            $result = "error";
        }

        return json_encode(array('message' => $result));

    }

}