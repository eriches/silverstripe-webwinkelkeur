<?php

use SilverStripe\Core\Config\Config;
use Hestec\WebwinkelKeur\Review;
use SilverStripe\Control\Email\Email;

class WwkRequest extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'Reviews'
    );

    public function test(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            /*$whitelabel_id = Config::inst()->get(__CLASS__, 'whitelabel_id');

            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://api-internet.whitelabeled.nl/v1/compare/'.$whitelabel_id.'?include_tv=true&include_phone=true&page=1&items_per_page=1', [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ]
            ]);
            $msgs = json_decode($response->getBody());


            foreach ($msgs->products as $node) {

                //echo $node->score_elements;

                foreach ($node->score_elements as $node) {

                    echo $node->type;

                }

            }*/

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function Reviews() {

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            $apikey = Config::inst()->get('WebwinkelKeur', 'apikey');
            $shopid = Config::inst()->get('WebwinkelKeur', 'shopid');

            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request('GET', 'https://dashboard.webwinkelkeur.nl/api/1.0/ratings.json?limit=100&id='.$shopid.'&code='.$apikey, [
                    'curl' => [
                        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                    ]
                ]);

            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error in Travelfeed ID: " . $feed->ID, $e->getMessage());
                $email->sendPlain();
                $error = true;
            }
            if (!isset($error)) {

                $msgs = json_decode($response->getBody());

                if (!empty($msgs->ratings)) {
                    foreach ($msgs->ratings as $node) {

                        if (Review::get()->filter(array('DateTime' => $node->created, 'Email' => $node->email))->count() == 0) {

                            $review = new Review();
                            $review->Name = $node->name;
                            $review->Comment = $node->comment;
                            $review->Rating = $node->rating;
                            $review->Email = $node->email;
                            $review->DateTime = $node->created;
                            $review->Enabled = false;
                            $review->write();

                        }


                    }

                }

            }
        }

    }

    public function City() {

        $Params = $this->getURLParams();

        if (isset($Params['ID']) && is_numeric($Params['ID'])) {

            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request('GET', 'https://api.hestec.xyz/CountryApi/City/'.$Params['ID'], [
                    'curl' => [
                        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                    ]
                ]);

            } catch (Exception $e) {
                //echo 'Caught exception: ', $e->getMessage(), "\n";
                //$email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at update Whitelabeled Energy", $e->getMessage());
                //$email->sendPlain();
                $error = true;
            }
            if (!isset($error)) {

                $msgs = json_decode($response->getBody(), true);

                if (!empty($msgs)) {

                    $city = City::get()->byID($Params['ID']);

                    if (empty($city)) {

                        $city = new City();
                        $city->ID = $msgs['ID'];

                    }

                    if (isset($msgs['NameEnglish'])) {
                        $city->NameEnglish = $msgs['NameEnglish'];
                    }
                    if (isset($msgs['NameLocal'])) {
                        $city->NameLocal = $msgs['NameLocal'];
                    }
                    if (isset($msgs['NameDutch'])) {
                        $city->NameDutch = $msgs['NameDutch'];
                    }
                    if (isset($msgs['SearchNamesValue'])) {
                        $city->SearchNames = $msgs['SearchNamesValue'];
                    }
                    if (isset($msgs['WikiDataId'])) {
                        $city->WikiDataId = $msgs['WikiDataId'];
                    }
                    if (isset($msgs['Latitude'])) {
                        $city->Latitude = $msgs['Latitude'];
                    }
                    if (isset($msgs['Longitude'])) {
                        $city->Longitude = $msgs['Longitude'];
                    }
                    if (isset($msgs['Timezone'])) {
                        $city->Timezone = $msgs['Timezone'];
                    }
                    if (isset($msgs['Population'])) {
                        $city->Population = $msgs['Population'];
                    }
                    if (isset($msgs['Region'])) {
                        $city->Region = $msgs['Region'];
                    }
                    if (isset($msgs['RegionCode'])) {
                        $city->RegionCode = $msgs['RegionCode'];
                    }
                    if (isset($msgs['ElevationMeters'])) {
                        $city->ElevationMeters = $msgs['ElevationMeters'];
                    }
                    if (isset($msgs['CountryID'])) {
                        $city->CountryID = $msgs['CountryID'];
                    }

                    $city->write();

                    return "updated";

                }
                return "no data";

            }

            return "error";

        }
        return "no ID";

    }

}
