<?php

namespace Hestec\WebwinkelKeur;

use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;

class ReviewPage extends \Page {

    private static $table_name = 'HestecWebwinkelKeurReviewPage';

    private static $db = array(
        'InvitationTitle' => 'Varchar(255)',
        'InvitationText' => 'HTMLText'
    );

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $InvitationTitleField = TextField::create('InvitationTitle', "InvitationTitle");
        $InvitationTextField = HTMLEditorField::create('InvitationText', "InvitationText");

        $fields->addFieldsToTab(
            'Root.Invitation', [
                $InvitationTitleField,
                $InvitationTextField
            ]
        );

        return $fields;
    }

}