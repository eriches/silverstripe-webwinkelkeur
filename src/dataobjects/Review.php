<?php

namespace Hestec\WebwinkelKeur;

use SilverStripe\ORM\DataObject;

class Review extends DataObject {

    private static $singular_name = 'Review';
    private static $plural_name = 'Reviews';

    private static $table_name = 'HestecWebwinkelKeurReview';

    private static $db = array(
        'Name' => 'Varchar(100)',
        'Comment' => 'Text',
        'Rating' => 'Int',
        'Email' => 'Varchar(100)',
        'DateTime' => 'Datetime',
        'Enabled' => 'Boolean'
    );

    //private static $default_sort='NameDutch';

    /*private static $has_many = array(
        'Cities' => City::class
    );*/

}