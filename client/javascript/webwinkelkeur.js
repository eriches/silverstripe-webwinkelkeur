function ReviewInvite(email, name, link, elem, elemform){

    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: link + 'ReviewInvite',
        data: {email: email, name: name},
        success: function (data) {
            if (data['message'] == 'success'){
                $(elem + ' .wwk-message').empty();
                $(elem + ' .wwk-message').removeClass('wwk-error');
                $(elemform).hide();
                $(elem + ' .wwk-message').append("Bedankt, je ontvangt een e-mail met een uitnodiging van WebwinkelKeur. Dit kan soms enkele uren duren.");
            }else{
                $(elem + ' .wwk-message').prepend("Controleer nog even je e-mailadres.");
            }
        },
        error: function (data) {
            $(elem + ' .wwk-message').empty();
            $(elem + ' .wwk-message').addClass('wwk-error');
            switch(data['message']) {
                case 'error':
                    $(elem + ' .wwk-message').prepend("Controleer nog even je e-mailadres.");
                    break;
                default:
                    $(elem + ' .wwk-message').prepend("Controleer nog even je e-mailadres.");
            }
        }
    })

};

// possibly place this in an existing "document.ready"
$(document).ready(function() {

    $('#Form_ReviewInviteForm_action_SubmitReviewInviteForm').click(function() {

        var email = $('#Form_ReviewInviteForm_Email').val();
        var name = $('#Form_ReviewInviteForm_Name').val();
        var link = $('#Form_ReviewInviteForm_Link').val();
        var elem = '#wwk-invite';
        var elemform = '#wwk-form';

        ReviewInvite(email, name, link, elem, elemform);
        return false;
    });

});
